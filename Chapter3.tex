\newpage \thispagestyle{empty}\vspace*{1in}\pagebreak
\chapter{Future work}
The work carried out up to the present stage was focused on the question: \textit{How to get the most out of an incorrectly specified model structure?} In order to finalise the study on this approach to phenomenological modelling, the methodology proposed in Section \ref{sec:chemometrics} will be applied on a simulated case study for identifying an approximated kinetic model of a catalytic system. More details are given in Section \ref{sec:future_work_1}. The following steps of the research project will mainly focus on the question: \textit{Is it possible to identify a systematic way for constructing phenomenological kinetic models embedding both experimental evidence and knowledge available on the system?} The systematic construction of deterministic laws describing kinetic phenomena relevant to the field of reaction engineering is assumed as objective for the study. The sources of uncertainty that are present in the study of kinetic phenomena will be decoupled and targeted independently. More details are given in Section \ref{sec:future_work_2}. The time allocation for the future research activities is presented in a Gantt chart in Table \ref{tab:ganttchart}.


\section{Study on the identification of an approximated model}
\label{sec:future_work_1}
The methodology presented in Section \ref{sec:methodology_chemometrics} was not tested on a complete case study including a design of experiment step. This is object of future work. The aim will be to test the proposed methodology on a case study simulated in silico. The considered system will be analogous to the one presented in Section \ref{sec:case_study_chemometrics}, i.e., a heterogeneous catalytic reaction performed in a tubular reactor with rectangular cross section where the catalyst is sputtered only on the bottom of the channel. The candidate model will involve the correct kinetics but will neglect diffusion resistances and it will assume that the catalyst is uniformly distributed in the reactor volume, i.e., radial gradients in concentrations and rates for the catalytic reactions are neglected (see Figure \ref{fig:candidate_reactor}). The true model instead will involve mass transfer resistances and it will consider the fact that the catalyst is present only as a sputtered layer in the bottom of the reactor channel (see Figure \ref{fig:true_reactor}).

The objective of the study is to demonstrate the applicability of the methodology proposed in Section \ref{sec:methodology_chemometrics} on a case study of practical interest for the automated identification of the diffusion limited regime of the reaction and the estimation of the reaction parameters using a misspecified model.

The application of the full procedure presented in the block-diagram in Figure \ref{fig:methodology_chemometrics} will also involve a MBDoE step for parameter precision within the domain of reliability of the candidate model. In Section \ref{sec:case_study_chemometrics}, it was shown that the model domain of reliability may be characterised by a very complicated geometry. The optimisation of a nonlinear objective function (i.e. the predicted Fisher information) subject to non-linear and non-convex constraints may be very challenging for available derivative-based optimisation solvers. Thus, for the experimental design purposes, adaptive sampling techniques will be employed \citep{boukouvala_derivative-free_2014}.

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{Future/candidate_reactor.png}
\caption{Candidate model for the reactor neglecting diffusion resistances and assuming uniform distribution of the catalyst across the reactor volume}
\label{fig:candidate_reactor}
\end{figure}

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{Future/true_reactor.png}
\caption{True model for the reactor considering diffusion resistances and the spatial location of the sputtered catalyst film.}
\label{fig:true_reactor}
\end{figure}

\section{Systematic construction of kinetic models}
\label{sec:future_work_2}
Mechanistic modelling is frequently regarded as an art \cite{bonvin_linking_2016}.  In fact, despite the extensive computational resources that are now available, in many contexts, model building still relies on a series of judicious choices made by thoughtful and experienced researchers \cite{bonvin_linking_2016}. A demonstration of the significant impact that human choice may have in the modelling activity was given by Berty \emph{et al.} in the \textit{"UCKRON-1"} test problem \cite{berty_uckron-1_1989}. In their work, an artificial kinetic model based on the known thermodynamics of methanol synthesis from carbon dioxide and hydrogen. The purpose was not to describe the real kinetic mechanism, but to develop a model to use as benchmark for generating data in silico. A dataset was generated simulating the system on a CSTR and collecting some of the data at conditions that were close to the thermal runaway of the reaction. The dataset was then sent to 19 different academic groups who were challenged to developing their own kinetics. The developed models were significantly different and despite the fact that were able to fit the given dataset satisfactorily, only few groups were able to predict the thermal runaway. The study not only stressed the necessity of a tight interaction between modelling and experimental activity for validating the candidate model, but also the need for a more systematic and comprehensive framework for: data pretreatment, data analysis, kinetic model development, targeted model-based design of experiments, model diagnosis and model enhancement.

The focus of the next steps of the research project will be on the systematic construction of models in the form (\ref{Eq:general_kinetic_model}), in which the time-space evolution of a population of chemical species (i.e. molecules) in a reacting mixture has to be properly described satisfying opportune physical constraints (i.e. thermodynamic feasibility). Diffusion phenomena will be neglected.

\begin{equation}
\frac{\partial C_i}{\partial t}+\nabla \cdot (C_i \textbf{v})=\sum_{j=1}^{N_R}\nu_{ij}r_j \quad i=1,...,N_C
\label{Eq:general_kinetic_model}
\end{equation}

In (\ref{Eq:general_kinetic_model}), \textbf{v} is the velocity of the moving fluid bulk. The identification of a model in the form (\ref{Eq:general_kinetic_model})  typically starts with the decision of the degree of lumping that it is necessary to consider in the reactor, i.e., the degree of detail of the mesh on which to solve equation (\ref{Eq:general_kinetic_model}) and momentum and energy balances. Only standard reactor types operated under ideal conditions will be considered, i.e., perfectly mixed batch, CSTR and PFR reactors. Only species for which concentration measurements can be obtained or inferred will be considered in the model. The following steps require: \textit{1}) the selection of the number of reactions $N_R$ to include in the model and the determination of an opportune form of the $N_C$x$N_R$-dimensional stoichiometric matrix $\boldsymbol{\upnu}=(\nu_{ij})$; \textit{2}) the functional form for the reaction rates of the $N_R$ reactions and; \textit{3}) the estimation of the non-measurable kinetic parameters from noisy data of mixture concentrations.

The typical approach consists of proposing a comprehensive statistical model including assumptions on stoichiometries, assumptions on the functional relationships for reaction rates and assumptions on the measurement noise. However, it is argued that such approach does not allow  for a practical diagnosis of the model structure making very challenging its subsequent improvement.

The kinetic model identification task will be initially divided into the independent identification of three sub-models in an attempt of decoupling the uncertainties present in the determination of: measurement errors, stoichiometry and reaction rates. Eventually the sub-models will be embedded into a unique statistical model for kinetic parameter estimation and statistical validation.

\subsection{Modelling of concentration measurements errors}
\label{sec:modelling_errors}
The typical approach that is adopted for identifying a model for the distribution of measurement errors involves the repetition of measurements at the same experimental conditions. From a repeated measurement it is possible to ascertain the error distribution. However, this approach presents several drawbacks:
\begin{itemize}
\item{it requires the repetition of the experiment, which necessitates time and resources;}
\item{it may be impossible to repeat the experiment at exactly the same conditions;}
\item{it does not consider that the kinetic behaviour of the system may change between an experiment and the other (a typical example from the field of catalysis is the change of kinetic behaviour in the system caused by catalyst deactivation);}
\end{itemize}  
A maximum likelihood method will be developed for inferring the distribution of measurement errors from the mass balance of atomic species without requiring the repetition of the trials. A first case of a catalytic system in which all the molecules present in the mixture can be measured will be considered and tested in silico assuming the presence of catalyst deactivation between two sequential experiments. A second case will be considered in which not all the molecules can be measured. A modified version of the approach will be tested with the aims of providing a quantification of the measurement errors and \textit{lumping} the non-detectable species as a single missing compound. Knowledge on the missing compound may be included in the kinetic model and would be beneficial for providing a more accurate representation of the measurable chemical species.

\begin{center}
\textbf{Objectives}
\end{center}
\begin{itemize}
\item{Discerning the measurement error type among common error class types, e.g.: constant error variance; constant relative variance; linear variance; heteroscedastic variance.}
\item{Obtaining information about the non-measurable components for improving the predictive capabilities of the final kinetic model.}
\end{itemize}


\subsection{Modelling of the stoichiometric matrix}
Target Factor Analysis (TFA) techniques were applied successfully for identifying the independent reactions of a reacting system and for confirming if a proposed target stoichiometry lies in the observed stoichiometric space \citep{bonvin_target_1990, amrhein_target_1999}. TFA presents three major drawbacks:
\begin{enumerate}
\item{it relies on the proper quantification of the measurement errors in the concentrations;}
\item{it does not take into account the thermodynamic feasibility of reaction pathways (i.e. a thermodynamic infeasible reaction pathway may be judged as compatible with the observed data);}
\item{it still relies on the presence of a chemist for proposing reasonable reaction pathways to test.}
\end{enumerate}
For addressing the first drawback, the models for measurement errors identified from a likelihood method based on the conservation of atomic species will be employed (see Section \ref{sec:modelling_errors}). For addressing the second drawback, it is then in the aims of the present research project to extend the TFA method for stoichiometry elucidation including thermodynamic constraints and the study of design of experiment techniques for reaction network identification. For addressing the third drawback, the possibility of employing Machine Learning techniques such as Artificial Neural Networks (ANNs) for elucidating the structure of the reaction network will be explored. Results from a preliminary study on the applicability of the method are reported in Appendix B. The reaction network proposed by the ANN may then be checked in terms of thermodynamic feasibility and consistency with the experimental data applying TFA methods.

\begin{center}
\textbf{Objectives}
\end{center}
\begin{itemize}
\item{Including thermodynamic constraints in TFA methodologies to obtain a reduced \textit{feasible} observed stoichiometric space.}
\item{Development of design of experiments methods for reaction networks elucidation.}
\item{Testing the applicability of machine learning technologies for supporting the identification of mechanistic models.}
\end{itemize}

\subsection{Modelling of the functional form of the reaction rates}
At this stage, the stoichiometric matrix is fixed for the construction of the functional form of the reaction rates $r_j$ with $j=1,...,N_R$. The starting point is the assumption that reaction rate laws are described by Arrhenius-type equations where the rate is only proportional to the concentration of reactants. The kinetic model parameters will then be estimated through a framework for approximated model identification such as the one presented in Section \ref{sec:chemometrics} for considering the possible presence of process model mismatch. Available knowledge on the measurement errors will be implemented in the maximum likelihood estimation. If the application of the method identifies areas of low model reliability in the input space (see, as an example, Figure \ref{fig:reliability_region}), the reason of the mismatch shall be diagnosed and amended. Multivariate analysis methods based on Principal Component Analysis were proposed in the literature for identifying the model components that are correlated to the cause of the mismatch \citep{meneghetti_methodology_2014}. However, these methods do not provide direct guidelines on which is the opportune way of amending the model structure to reduce and eventually eliminate the model-process discrepancy. A method based on the selective fitting of experimental data, namely the \textit{focus} method, will be studied to highlight the presence of hidden state variables in the candidate model parameters and guiding the amendment of the model structure itself. A preliminary study where the \textit{focus} method is applied on a biomass growth model with incorrect structure is given in Appendix C.

\newpage
\begin{center}
\textbf{Objectives}
\end{center}
\begin{itemize}
\item{Development of a framework for automated model structure diagnosis and model amendment with subsequent implementation in a computational framework. The method shall be regarded as a genetic programming approach in which model weaknesses are detected and amended in an iterative fashion. An analogy with the Lamarckian model of biological evolution may be appreciated.}
\item{Comparison of the proposed method with the performance of conventional genetic algorithms, inspired by Darwin's theory of evolution \citep{salhi_parallel_1998}.}
\end{itemize}

\section{Additional side activities}
In the course of the research project time will be also allocated to some additional side activities:
\begin{enumerate}
\item{independent study of scientific literature on the topic and the consideration of alternative promising approaches to the systematic modelling of kinetics;}
\item{collaboration with other research groups for the application and test of the developed methodologies on lab-scale setups.}
\end{enumerate}

\section{Publications and conferences}
\label{sec:planned_conferences}
The work and the studies that will be conducted in the next two years of the research project will gradually build the main body of the final PhD Thesis and will be also delivered to the scientific community through a number of scientific publications and through the participation to relevant conferences.

\begin{center}
\textbf{Planned publications}
\end{center}
\begin{enumerate}
\item{April 2018: Complete study on the identification of an approximated model (see Section \ref{sec:future_work_1}).}
\item{July 2018: Study on the application of a maximum likelihood approach for modelling the concentration measurements errors (see Section \ref{sec:future_work_2}.1).}
\item{January 2019: Study on the applicability of Artificial Neural Networks on the elucidation of the reaction network (\ref{sec:future_work_2}.2).}
\item{July 2019: Study on the automated identification of kinetic models through the application of a Lamarckian genetic algorithm implementing the \textit{focus} method (see Section {\ref{sec:future_work_2}.3}).}
\end{enumerate}

\begin{center}
\textbf{Planned conferences}
\end{center}
\begin{enumerate}
\item{July 2018: $18^{\textrm{th}}$ IFAC Symposium on System Identification, SYSID 2018, Stockholm. Work related to the identification of approximated models will be presented. Submission deadline: December the $8^{\textrm{th}}$ 2017.}
\item{January 2019: UK Catalysis Conference, Loughborough (still to confirm). Presentation of the study on the automated identification of the diffusion limited regime (see Section \ref{sec:future_work_1}). Approximated submission deadline: September 2018.}
\item{2019: $29^{\textrm{th}}$ European Symposium on Computer Aided Process Engineering (place and important dates yet to be released). Presentation of the study on the automated identification of the reaction network or the application of the focus method for the identification of the functional form of the reaction rates (see Section \ref{sec:future_work_2}). Approximated submission deadline: October 2018.}
\end{enumerate}

\newpage

\begin{landscape}

\begin{table}

\caption{Gantt chart showing the time allocation for the future research activities.}
\renewcommand{\baselinestretch}{1.1}
\center
\resizebox*{1.0\textheight}{!}{
\begin{ganttchart}[hgrid,vgrid,newline shortcut=true,
            bar label node/.append style={align=right}]{1}{24}
    \gantttitle[]{2017}{3}                 % title 2
    \gantttitle[]{2018}{12}
    \gantttitle[]{2019}{9} \\              
    \gantttitle{Oct-Dec}{3}                      % title 3
    \gantttitle{Jan-Mar}{3}
    \gantttitle{Apr-Jun}{3}
    \gantttitle{Jul-Sep}{3}
    \gantttitle{Oct-Dec}{3}                      % title 3
    \gantttitle{Jan-Mar}{3}
    \gantttitle{Apr-Jun}{3}
    \gantttitle{Jul-Sep}{3}	\\
\ganttgroup{Study on the identification of approximated models}{1}{6} \\
\ganttbar{Optimal MBDoE constrained to model domain of reliability}{1}{3} \\
\ganttlinkedbar{Application of the full procedure presented in\ganttalignnewline Section \ref{sec:methodology_chemometrics} to the identification of an incorrect model}{4}{6} \\
\ganttmilestone{Publication: Complete study on the identification of an approximated model}{6}\\
\ganttgroup{Systematic construction of kinetic models}{7}{21} \\
\ganttbar{Likelihood method for modelling errors \ganttalignnewline in concentration measurements}{7}{9} \ganttnewline
\ganttmilestone{Publication: Modelling of concentration measurements errors}{9}\\
\ganttlinkedbar{Application of ANNs for the elucidation of the reaction network}{11}{15} \\

\ganttmilestone{Publication: Applicability of ANNs on the mechanistic modelling of kinetics}{15}\\

\ganttbar{Study of multivariate methods for the identification of the reaction network}{10}{14} \\

\ganttlinkedbar{Study of multivariate methods for model diagnosis and\ganttalignnewline development of the focus method}{16}{21}\\
\ganttmilestone{Publication: Lamarckian genetic algorithm for reaction rates construction}{21}\\
\ganttgroup{Completion of PhD Thesis and final submission}{22}{24} \\

\ganttmilestone{Planned conferences, submission deadlines (see Section \ref{sec:planned_conferences} for more detail)}{3}\ganttmilestone{}{12}\ganttmilestone{}{13}\\

\ganttbar{Literature update}{1}{24}\\
\ganttbar{Collaborations with other research groups}{1}{24}

\ganttlink{elem2}{elem3}
\ganttlink{elem3}{elem10}

\ganttlink{elem5}{elem6}



\ganttlink{elem6}{elem7}
\ganttlink{elem7}{elem8}
\ganttlink{elem6}{elem9}
\ganttlink{elem8}{elem10}
\ganttlink{elem10}{elem11}


\end{ganttchart}}


\label{tab:ganttchart}
\end{table}

\end{landscape}
\newpage \thispagestyle{empty} \vspace*{1in} \pagebreak

\label{chapterlabel3}

% This just dumps some pseudolatin in so you can see some text in place.
